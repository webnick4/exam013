#!/bin/bash

REL_PATH=$(dirname "$0")
cd ${REL_PATH}
DIR="${PWD}"

echo "Current dir is: $PWD"

cd ${DIR}/../client
yarn
pm2 start "yarn start" --name cafe-critic-frontend

cd ${DIR}/../api
yarn
yarn seed
pm2 start "yarn start" --name cafe-critic-backend
 
