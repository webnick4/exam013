import React, {Fragment} from 'react';
import {MenuItem, Nav, NavDropdown, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const UserMenu = ({user, logout}) => {
  const navTitle = (
    <Fragment>
      Hello, <b>{user.username}</b>!
    </Fragment>
  );

  return (
    <Fragment>
      <Nav pullRight>
        <NavDropdown title={navTitle} id="user-menu">
          <MenuItem onClick={logout}>Logout</MenuItem>
        </NavDropdown>
      </Nav>
      <Nav pullRight>
        <LinkContainer to="/add-new-place">
          <NavItem>Add new place</NavItem>
        </LinkContainer>
      </Nav>
    </Fragment>
  )
};

export default UserMenu;