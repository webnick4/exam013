import React, {Component} from 'react';
import {Button, Col, Form, FormGroup} from "react-bootstrap";
import FormElement from "../UI/Form/FormElement";

class PlaceForm extends Component {
  state = {
    title: '',
    description: '',
    image: ''
  };

  submitHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.onSubmit(formData);
  };

  onChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };

  render() {
    return (
      <Form horizontal onSubmit={this.submitHandler}>
        <FormElement
          propertyName="title"
          title="Title"
          type="text"
          value={this.state.title}
          changeHandler={this.onChangeHandler}
          required
        />

        <FormElement
          propertyName="description"
          title="Description"
          type="textarea"
          value={this.state.description}
          changeHandler={this.onChangeHandler}
          required
        />

        <FormElement
          propertyName="image"
          title="Main photo"
          type="file"
          changeHandler={this.fileChangeHandler}
        />

        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button bsStyle="primary" type="submit">Submit new place</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

export default PlaceForm;
