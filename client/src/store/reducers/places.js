import {
  ADD_NEW_PLACE_FAILURE,
  GET_PLACE_BY_ID_SUCCESS, GET_PLACE_BY_ID_FAILURE,
  GET_PLACES_SUCCESS, GET_PLACES_FAILURE
} from "../actions/actionTypes";

const initialState = {
  places: [],
  place: [],
  placeError: null,
  placesError: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_NEW_PLACE_FAILURE:
      return { ...state, placeError: action.error };
    case GET_PLACE_BY_ID_SUCCESS:
      return { ...state, place: action.place, placeError: null};
    case GET_PLACE_BY_ID_FAILURE:
      return { ...state, placeError: action.error };
    case GET_PLACES_SUCCESS:
      return {...state, places: action.places, placesError: null};
    case GET_PLACES_FAILURE:
      return {...state, placesError: action.error};
    default:
      return state;
  }
};

export default reducer;