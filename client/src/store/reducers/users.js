import {
  LOGIN_USER_FAILURE, LOGIN_USER_SUCCESS,
  LOGOUT_USER,
  CREATE_USER_ERROR, CREATE_USER_SUCCESS
} from "../actions/actionTypes";

const initialState = {
  createError: null,
  loginError: null,
  user: null,
  token: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_USER_SUCCESS:
      return {...state, createError: null};
    case CREATE_USER_ERROR:
      return {...state, createError: action.error};
    case LOGIN_USER_SUCCESS:
      return {...state, user: action.user, token: action.token, loginError: null};
    case LOGIN_USER_FAILURE:
      return {...state, loginError: action.error};
    case LOGOUT_USER:
      return {...state, user: null};
    default:
      return state;
  }
};

export default reducer;