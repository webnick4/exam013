import axios from '../../axios-api';
import { NotificationManager } from 'react-notifications';
import { push } from 'react-router-redux';

import {
  ADD_NEW_PLACE_FAILURE, ADD_NEW_PLACE_SUCCESS,
  GET_PLACE_BY_ID_FAILURE, GET_PLACE_BY_ID_SUCCESS,
  GET_PLACES_FAILURE, GET_PLACES_SUCCESS
} from "./actionTypes";

export const getPlacesSuccess = places => {
  return { type: GET_PLACES_SUCCESS, places };
};

export const getPlacesFailure = error => {
  return { type: GET_PLACES_FAILURE, error };
};

export const getBook = () => {
  return dispatch => {
    return axios.get('/places').then(
      response => {
        dispatch(getPlacesSuccess(response.data));
      },
      error => {
        dispatch(getPlacesFailure(error.response.data));
      }
    );
  };
};

const addNewPlaceSuccess = () => {
  return { type: ADD_NEW_PLACE_SUCCESS };
};

const addNewPlaceFailure = error => {
  return { type: ADD_NEW_PLACE_FAILURE, error };
};

export const addNewPlace = placeData => {
  return dispatch => {
    return axios.post('/places', placeData).then(
      response => {
        dispatch(addNewPlaceSuccess());
        dispatch(push('/'));
      },
      error => {
        dispatch(addNewPlaceFailure(error.response.data));
      }
    );
  };
};

const getPlaceByIdSuccess = place => {
  return { type: GET_PLACE_BY_ID_SUCCESS, place };
};

const getPlaceByIdFailure = error => {
  return { type: GET_PLACE_BY_ID_FAILURE, error };
};

export const getPlaceById = barcode => {
  return dispatch => {
    axios.get(`/books/barcode/${barcode}`).then(
      response => {
        dispatch(getPlaceByIdSuccess(response.data));
      },
      error => {
        dispatch(getPlaceByIdFailure(error.response.data));
        NotificationManager.error(error.response.data.error);
      }
    );
  };
};