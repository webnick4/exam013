import axios from '../../axios-api';
import {NotificationManager} from 'react-notifications';
import {push} from 'react-router-redux';
import {
  LOGIN_USER_FAILURE, LOGIN_USER_SUCCESS,
  LOGOUT_USER,
  CREATE_USER_ERROR, CREATE_USER_SUCCESS
} from "./actionTypes";

const createUserSuccess = () => {
  return {type: CREATE_USER_SUCCESS};
};

const createUserFailure = error => {
  return {type: CREATE_USER_ERROR, error};
};

export const createUser = userData => {
  return dispatch => {
    return axios.post('/users', userData).then(
      response => {
        dispatch(createUserSuccess());
        dispatch(push('/'));
        NotificationManager.success('Регистрация пользователя прошла успешно!');
      },
      error => {
        dispatch(createUserFailure(error.response.data));
      }
    );
  };
};

const loginUserSuccess = (user, token) => {
  return {type: LOGIN_USER_SUCCESS, user, token};
};

const loginUserFailure = error => {
  return {type: LOGIN_USER_FAILURE, error};
};

export const loginUser = userData => {
  return dispatch => {
    return axios.post('/users/sessions', userData).then(
      response => {
        dispatch(loginUserSuccess(response.data.user, response.data.token));
        dispatch(push('/'));
        NotificationManager.success(response.data.message);
      },
      error => {
        const errorObj = error.response ? error.response.data : {error: 'Нет подключения к интернету!'};
        dispatch(loginUserFailure(errorObj));
      }
    )
  }
};

export const logoutUser = () => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;
    const headers = {'Token': token};
    axios.delete('/users/sessions', {headers}).then(
      response => {
        dispatch({type: LOGOUT_USER});
        dispatch(push('/'));
        NotificationManager.success('Успешно вышли из приложения!');
      },
      error => {
        NotificationManager.error('Выйти из приложения не удалось!');
      }
    );
  }
};

export const logoutExpiredUser = () => {
  return dispatch => {
    dispatch({type: LOGOUT_USER});
    dispatch(push('/login'));
    NotificationManager.error('Ваша сессия закончилась, пожалуйста войдите в приложение по новой!');
  }
};