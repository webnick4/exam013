import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import {addNewPlace} from "../../store/actions/places";
import PlaceForm from "../../components/PlaceForm/PlaceForm";

class AddNewPlace extends Component {
  createPlace = placeData => {
    this.props.onPlaceCreated(placeData);
  };

  render() {
    return (
      <Fragment>
        <PageHeader>New product</PageHeader>
        <PlaceForm
          onSubmit={this.createPlace}
        />
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  onPlaceCreated: placeData => {
    return dispatch(addNewPlace(placeData))
  }
});


export default connect(null, mapDispatchToProps)(AddNewPlace);