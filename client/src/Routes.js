import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";

import Places from './containers/Places/Places';
import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register";
import AddNewPlace from "./containers/AddNewPlace/AddNewPlace";

const ProtectedRoute = ({isAllowed, ...props}) => (
  isAllowed ? <Route {...props}/> : <Redirect to="/login" />
);

const Routes = ({user}) => (
  <Switch>
    <Route path="/" exact component={Places}/>

    <ProtectedRoute
      isAllowed={user && (user.role === "admin" || user.role === "user")}
      path="/add-new-place"
      exact
      component={AddNewPlace}
    />

    <Route path="/register" exact component={Register}/>
    <Route path="/login" exact component={Login}/>
    <Route
      render={() => <h1 style={{textAlign: "center"}}>Page not found</h1>}
    />
  </Switch>
);

export default Routes;