const jwt = require('jsonwebtoken');
const User = require('../models/User');
const config = require('../config');

const auth = async (req, res, next) => {
  const token = req.get('Token');

  if (!token) {
    return res.status(401).send({message: 'У вас нет токена для аутентификации'});
  }

  let tokenData;

  try {
    tokenData = jwt.verify(token, config.jwt.secret);
  } catch (e) {
    console.log(e);
    return res.status(401).send({message: 'Неверный токен'});
  }

  const user = await User.findById(tokenData.id);

  if (!user) {
    return res.status(401).send({message: 'Такого пользователя не существует'});
  }

  req.user = user;

  next();
};

module.exports = auth;