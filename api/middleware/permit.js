const permit = (...roles) => {
    return (req, res, next) => {
        if (!req.user) {
            return res.status(401).send({message: 'Не авторизован!'});
        }

        if (!roles.includes(req.user.role)) {
            return res.status(403).send({message: 'Нет доступа!'});
        }

        next();
    };
};

module.exports = permit;

