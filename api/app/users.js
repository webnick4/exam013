const express = require('express');

const User = require('../models/User');
const auth = require('../middleware/auth');

const createRouter = () => {
  const router = express.Router();

  router.post('/', (req, res) => {
    const user = new User({
      username: req.body.username,
      password: req.body.password
    });

    user.save()
      .then(user => res.send(user))
      .catch(error => res.status(400).send(error))
  });

  router.post('/sessions', async (req, res) => {
    const user = await User.findOne({username: req.body.username});

    if (!user) {
      return res.status(400).send({error: 'Пользователь с таким именем не найден!'});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
      return res.status(400).send({error: 'Неверный пароль!'});
    }

    const token = user.generateToken();

    return res.send({message: 'Аутентификация успешно выполнена!', user, token});
  });

  router.post('/verify', auth, (req, res) => {
    res.send({message: 'Token valid'});
  });

  router.delete('/sessions', async (req, res) => {
    const token = req.get('Token');
    const success = {message: 'Удачный выход из приложения!'};

    if (!token) return res.send(success);

    const user = await User.findOne({token});

    if (!user) return res.send(success);

    user.generateToken();
    await user.save();

    return res.send(success);
  });

  return router;
};

module.exports = createRouter;