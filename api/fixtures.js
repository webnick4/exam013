const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
  try {
    await db.dropCollection('users');
  } catch (e) {
    console.log('Collections were not present, skipping drop...');
  }

  await User.create({
    username: 'User',
    password: '123',
    role: 'user'
  }, {
    username: 'Admin',
    password: '123',
    role: 'admin'
  });

  db.close();
});