const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, '/public/uploads'),
  db: {
    url: 'mongodb://localhost:27017',
    name: 'cafe-critic'
  },
  jwt: {
    secret: 'some kinda very secret string',
    expires: 15600
  }
};

